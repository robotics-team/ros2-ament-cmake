#include <newbarlib/header.h>
#include <iostream>

namespace newbarlib
{

void bar()
{
    newfoolib::foo();
    std::cout << "Bar!\n";
}

}
