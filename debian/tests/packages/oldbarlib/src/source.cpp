#include <oldbarlib/header.h>
#include <iostream>

namespace oldbarlib
{

void bar()
{
    oldfoolib::foo();
    std::cout << "Bar!\n";
}

}
